#/bin/bash
set -e
set -x
netbootdir=/var/lib/tftpboot/er/plugins/netboot.xyz/
wget https://boot.netboot.xyz/ipxe/netboot.xyz.iso
mkdir $netbootdir/cdrom
mount -o loop netboot.xyz.iso $netbootdir/cdrom
cp -r $netbootdir/cdrom/ipxe.krn .
umount $netbootdir/cdrom/
rm -rf netboot.xyz.iso
